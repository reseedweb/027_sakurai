<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">お客様の声</h2>	
	<div class="box-content clearfix"><!-- begin box-content -->
		<p>
			メナードフェイシャルサロン Lei Lilium(レイリリウム)へご来店くださったお客様から、貴重なお声をいただきま
			した。Lei Liliumでエステを受けた感想など、お客様の生のお声をご紹介いたします。<br />
			(※こちらに掲載しているものは、個人の感想です。施術の効果には個人差があります。)
		</p>
	</div><!-- end box-content -->
</div><!-- end primary-row -->
		<?php
	    $queried_object = get_queried_object();	    
	    ?>
	    <?php $posts = get_posts(array(
	        'post_type'=> $queried_object->name,
	        'posts_per_page' => get_query_var('posts_per_page'),
	        'paged' => get_query_var('paged'),
	    ));
	    ?>
	    <?php $i = 0; ?>
		<?php foreach($posts as $p) :  ?>	        
		    <div class="voice-post-content">
	        	<div class="voice-post-title">
					<?php echo $p->post_title; ?>
				</div>				
				<p class="image"><?php echo get_the_post_thumbnail( $p->ID,'small'); ?></p>
				<p class="voice-title"><?php echo $p->customer_title; ?></p>
                <div class="voice-text">
					<?php echo $p->customer_text; ?>						                    
                </div>				
	        </div>	        		
	    <?php endforeach; ?>	    		
	    <div class="primary-row">
	        <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	    </div>
	    <?php wp_reset_query(); ?>
		

<?php get_footer(); ?>