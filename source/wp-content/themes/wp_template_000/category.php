<?php get_template_part('header'); ?>
<div class="primary-row">
    <div id="category1">
        <h2><span class="border"><?php single_cat_title('',true); ?></span></h2>
        <?php query_posts(array( 'paged' => get_query_var('paged') )); ?>
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>    
            <!-- do stuff ... -->
            <div class="primary-row">            
                <h3><?php the_title(); ?></h3>            
                <div class="post-row-content">                
                    <div class="post-row-meta">
                        <i class="fa fa-clock-o"><?php the_time('l, F jS, Y'); ?></i>
                        <i class="fa fa-tags"></i><?php the_category(' , ', get_the_id()); ?>
                        <i class="fa fa-user"></i><?php the_author_link(); ?>
                    </div>
                    <div class="post-row-description"><?php the_excerpt(); ?></div>
                    <p class="text-right"><a href="<?php the_permalink(); ?>">Readmore</a></p>               
                </div>
            </div>
            <?php endwhile; ?>    
            <div class="primary-row">
                <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
            </div>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </div>
</div>
<?php get_template_part('footer'); ?>