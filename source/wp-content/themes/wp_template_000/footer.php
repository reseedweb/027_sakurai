
                                </main><!-- end primary -->
                                <aside class="sidebar"><!-- begin sidebar -->
                                    <?php if(is_page('blog') || is_category() || is_single()) : ?>
                                        <?php
                                        $queried_object = get_queried_object();                                
                                        $sidebar_part = 'blog';
                                        if(is_tax() || is_archive()){                                    
                                            $sidebar_part = '';
                                        }                               

                                        if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
                                            $sidebar_part = '';
                                        }   
                                                
                                        if($queried_object->taxonomy == 'category'){                                    
                                            $sidebar_part = 'blog';
                                        }                 
                                        ?>
                                        <?php get_template_part('sidebar',$sidebar_part); ?>  
                                    <?php else: ?>
                                        <?php get_template_part('sidebar'); ?>  
                                    <?php endif; ?>                                    
                                </aside>
                            </div><!-- end two-cols -->
                        </div><!-- end col -->
                    </div><!-- end row -->
                </div><!-- end container -->            
            </section><!-- end main-content -->

            <footer><!-- begin footer -->
                <div class="container"><!-- begin container -->
                    <div class="row clearfix"><!-- begin row -->
                        <div class="col-md-18"><!-- begin col -->
                            <div class="footer-content clearfix">
								<div class="footer-logo">
									<a href="<?php bloginfo('url'); ?>/">
										<img alt="footer-logo" src="<?php bloginfo('template_url');?>/img/common/footer_logo.png" /> 									
									</a>
								</div> 
								<div class="footer-tel">
									<img alt="footer-tel" src="<?php bloginfo('template_url');?>/img/common/footer_tel.png" /> 									
								</div> 
								<div class="footer-con">
									<a href="<?php bloginfo('url'); ?>/contact">
										<img alt="footer-con" src="<?php bloginfo('template_url');?>/img/common/footer_con.png" /> 								
									</a>
								</div> 
                            </div>
							<div class="copyright text-right">
                                Copyright© メナードフェイシャルサロンLei Lilium. All Rights Reserved.
                            </div>
                        </div><!-- end col -->
                    </div><!-- end row -->
                </div><!-- end container -->            
            </footer><!-- end footer -->            

        </div><!-- end wrapper -->
        <?php wp_footer(); ?>
        <!-- js plugins -->
        <script type="text/javascript">

            jQuery(document).ready(function(){               
                jQuery('.bxslider').bxSlider({
					mode: 'fade',
					pause: 3000,
					speed: 2000,
					controls: false,
					auto: true,
					autoControls: false,
                });
                jQuery('.darktooltip').darkTooltip({
                    theme : 'light'
                });                
            }); 
        </script>                
    </body>
</html>