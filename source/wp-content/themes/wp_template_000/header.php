<!DOCTYPE html>
<html>
    <head>
        <!-- meta -->        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- title -->
        <title><?php wp_title( '|', true); ?></title>
        <meta name="robots" content="noindex,follow,noodp" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11" />                
        
        <!-- global javascript variable -->
        <script type="text/javascript" language="javascript">
            var CONTAINER_WIDTH = '1090px';
            var CONTENT_WIDTH = '1060px';
            var BASE_URL = '<?php bloginfo('url'); ?>';
            var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
            var CURRENT_MODULE_URI = '';
            Date.now = Date.now || function() { return +new Date; };            
        </script>        
        <!-- Bootstrap -->
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.min.css" rel="stylesheet" />   
        <!-- fontawesome -->
        <link href="<?php bloginfo('template_url'); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]-->        
        
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.plugins.js" type="text/javascript"></script>               
        <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" type="text/javascript"></script>        

        <link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />
        <script src="<?php bloginfo('template_url'); ?>/js/config.js" type="text/javascript"></script>        
        <?php wp_head(); ?>
    </head>
    <body>     
        <div id="screen_type"></div>
        
        <div id="wrapper"><!-- begin wrapper -->             

            <header><!-- begin header -->
                <div class="container"><!-- begin container -->
                    <div class="row clearfix"><!-- begin row -->
                        <div class="col-md-18"><!-- begin col -->
							<h1 class="top-title">
								奈良県 桜井市 エリアにあるフェイシャルエステサロン
							</h1>
                            <div class="header-content clearfix"><!-- begin header-content --> 
                                <div class="header-logo">
                                    <a href="<?php bloginfo('url'); ?>/">
                                        <img alt="logo" src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" />
                                    </a>
                                </div><!-- end header-logo -->
								<div class="header-tel">
                                    <img alt="logo" src="<?php bloginfo('template_url'); ?>/img/common/header_tel.jpg" />                                    
                                </div><!-- end header-tel -->
								<div class="header-con">
                                    <a href="<?php bloginfo('url'); ?>/contact">
                                        <img alt="logo" src="<?php bloginfo('template_url'); ?>/img/common/header_con.jpg" />
                                    </a>
                                </div><!-- end header-con -->
                            </div><!-- end header-content -->                            
                        </div><!-- end col -->
                    </div><!-- end row -->
                </div><!-- end container --> 				
            </header><!-- end header -->                                    

            <?php if(is_front_page()): ?>
            <section id="feature">    
				<?php get_template_part('part','mainslide'); ?>						
                <?php get_template_part('part','nav'); ?>
            </section>
            <?php else : ?>
			<section id="page-feature">
                <div class="container">
					<div class="row">
						<div class="col-md-18">
							<div class="page-feature-content">
								<?php if(is_archive('voice') || is_single()) : ?>                                
									<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_top_img.png" alt="page feature" />
								<?php elseif(is_page('standard')) : ?>
									<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_top_img.png" alt="page feature" />								
								<?php elseif(is_page('thanks')) : ?>
									<img src="<?php bloginfo('template_url'); ?>/img/content/thanks_content_top_img.png" alt="page feature" />
								<?php elseif(is_page('blog')) : ?>
									<img src="<?php bloginfo('template_url'); ?>/img/content/blog_content_top_img.png" alt="page feature" />
								<?php elseif(is_page('feature')) : ?>
									<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_top_img.png" alt="page feature" />
								<?php elseif(is_page('contact')) : ?>
									<img src="<?php bloginfo('template_url'); ?>/img/content/contact_content_top_img.png" alt="page feature" />
								<?php elseif(is_page('lesson')) : ?>
									<img src="<?php bloginfo('template_url'); ?>/img/content/lesson_content_top_img.png" alt="page feature" />
								<?php elseif(is_page('faq')) : ?>
									<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_top_img.png" alt="page feature" />								
								<?php elseif(is_page('option')) : ?>
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_top_img.png" alt="page feature" />
								<?php elseif(is_page('info')) : ?>
									<img src="<?php bloginfo('template_url'); ?>/img/content/info_content_top_img.png" alt="page feature" />
								<?php elseif(is_page('trial')) : ?>
									<img src="<?php bloginfo('template_url'); ?>/img/content/trial_content_top_img.png" alt="page feature" />
								<?php elseif(is_page('therapist')) : ?>
									<img src="<?php bloginfo('template_url'); ?>/img/content/therap_content_top_img.png" alt="page feature" />									
								<?php else : ?>
									<img src="<?php bloginfo('template_url'); ?>/img/content/page_feature_img.png" alt="page feature" />								
								<?php endif; ?>                        												
							</div>
						</div>
					</div>
				</div>                				
            </section>                          
			<?php get_template_part('part','nav'); ?>
            <?php endif; ?>            

            <section id="main-content"><!-- begin main-content -->
                <div class="container"><!-- begin container -->
                    <div class="row clearfix"><!-- begin row -->
                        <div class="col-md-18"><!-- begin col -->
                            <div class="two-cols-left clearfix"><!-- begin two-cols -->
                                <main class="primary"><!-- begin primary -->
       