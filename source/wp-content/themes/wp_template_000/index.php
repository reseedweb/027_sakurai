<?php get_header(); ?>
<div class="primary-row">
	<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-content-info1">
		<div class="title">
			奈良県・桜井市エリアの<br/>
			アットホームなエステサロン
		</div>
		<div class="text ln2em">
			奈良県・桜井市にあるエステサロン「Lei Lilium(レイリリウ
			ム)」は、本格的なエステをリーズナブルな料金でご提供す
			るメナードフェイシャルエステサロンです。<br/>
			高い技術を兼ねそろえたスタッフが、お客様一人ひとりのお
			悩みを丁寧にカウンセリングし、お身体やお肌の状態に合わ
			せた最高の技術とケアをご提供いたします。ぜひ、お気軽に
			お問い合わせ・ご予約ください。
		</div>
	</div><!-- .top-content-info1 -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-content-info2">		
		<a href="<?php bloginfo('url'); ?>/trial">
			<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img2.jpg" alt="top" />
			<div class="text ln15em">	
				はじめてLei LIlium(レイリリウム) をご利用になるお客様のために、お得なトライアルチケットをご用意いたしました。
			</div>			
		</a>
	</div><!-- .top-content-info3 -->	
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->	
	<div class="top-content-info3">		
		<a href="<?php bloginfo('url'); ?>/lesson">
			<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img3.jpg" alt="top" />
			<div class="text ln15em">	
				美しいお肌を手に入れるためには毎日のお手入れが重要です！
				イルネージュを使用して、洗顔の方法からメイクのレッスンま
				で、正しいお手入れ方法を学ぶことができます。
			</div>			
		</a>
	</div><!-- .top-content-info3 -->	
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="h2_title">メニューのご案内</h2>
    <div class="top-box-text">
        Lei lilium(レイリリウム)では、２種類のコースでお客様のキレイを実現いたします。<br />
		はじめてメナードのエステをされる方はまずはスタンダードコースをお試しください。
    </div>
    <div class="box-content-top"><!-- begin box-content-top -->
        <div class="box-col"><!-- box-col -->
            <img src="<?php bloginfo('template_url'); ?>/img/content/top_content_box1.png" alt="top" />
			<h3 class="top-box-title">
				スタンダードコース
			</h3>
			<div class="top-box-text">
				「イルネージュ」 「薬用リシアル」 「つき華」の
				３つのコースから選べるフェイシャルエステ
				メニュー。リンパの流れやお肌の皮脂汚れな
				どをケアする11ステップのコースです。 
			</div> 
			<div class="top-box-btn">
				<a href="<?php bloginfo('url'); ?>/standard">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_box_btn.jpg" alt="top" />
				</a>
			</div>
		</div><!-- end box-col -->
		<div class="box-col"><!-- box-col -->
            <img src="<?php bloginfo('template_url'); ?>/img/content/top_content_box2.png" alt="top" />
			<h3 class="top-box-title">
				オプションメニュー
			</h3>
			<div class="top-box-text">
				スタンダードコースと合わせていただくことで
				あなたのお肌の悩みを集中ケア！<br />
				一人ひとりのお肌の悩みに合わせたパックをご
				用意しております。
			</div> 
			<div class="top-box-btn">
				<a href="<?php bloginfo('url'); ?>/option">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_box_btn.jpg" alt="top" />
				</a>
			</div>
		</div><!-- end box-col -->
    </div><!-- end box-content-top -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-flow-content"><!-- begin top-flow-content -->
		<div class="flow-text">
			お客様に安心してご利用いただくため、レイリリウムでは6つの安心をお約束します。
		</div>
		<ul class="clearfix">
			<li>
				施術料と商品代金以外のお金は一切 いただいておりません。
			</li>
			<li>
				お客様のご要望に応じてお選びいただける料金システムです。
			</li>
			<li>
				無理な化粧品販売や勧誘などは行っておりません。
			</li>
		</ul>  
		<ul class="clearfix last-child">
			<li>
				当サロンは完全予約制ですので、待ち時間はかかりません。
			</li>
			<li>
				駐車場スペースがございますので、お車でも安心してご来店いただけます。
			</li>
			<li>
				各コースのエステメニューがお得にご利用頂けるサービスをご用意。
			</li>
		</ul>  
	<p class="text-center">
		<a href="<?php bloginfo('url'); ?>/feature">
			<img src="<?php bloginfo('template_url'); ?>/img/content/top_flow_btn.jpg" alt="top" />
		</a>
	</p>
    </div><!-- end top-flow-content -->	
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
    <div class="primary-row clearfix">
        <h2 class="h2_title">ブログ新着情報</h2>
        <div id="part-blog">
            <div>
                <table>                    
                    <colgroup>
                        <col>                        
                        <col>
                    </colgroup>                                        
                    <?php
                    //http://www.wpbeginner.com/beginners-guide/what-why-and-how-tos-of-creating-a-site-specific-wordpress-plugin/
                        $loop = new WP_Query('showposts=5&orderby=ID&order=DESC');
                        if($loop->have_posts()):
                    ?>
                        
                        <?php while($loop->have_posts()): $loop->the_post(); ?>
                            <tr>                
                                <th><?php the_time('Y/m/d'); ?></th>
                                <td><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>                             
                            </tr>                                                        
                        <?php endwhile; wp_reset_postdata();?>                        
                    <?php else: ?>
                        <tr><td colspan="3">No recent posts yet!</td></tr>
                    <?php endif; ?>                    
                </table>
            </div>
        </div>
    </div>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FFacebookDevelopers&amp;width=240&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=282914515196756" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:750px; height:290px;" allowTransparency="true"></iframe>	   
</div><!-- end primary-row -->

</div>
<?php get_footer(); ?>