<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>                        	
	<div class="primary-row clearfix">
		<h2 class="h2_title">ブログ</h2>           
		<p>
			Lei Liliumのオフィシャルブログです。サロンのことや施術のことなど、情報発信いたします。
		</p>
	</div>	
    <?php query_posts(array( 'paged' => get_query_var('paged') )); ?>
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>    
        <!-- do stuff ... -->
        <div class="primary-row clearfix"><!-- begin primary-row -->			
            <div class="post-row-content">                
                <div class="post-info">
                    <h2 class="post-title">
						<?php the_title(); ?>
					</h2>
					<div class="post-time">
						<?php the_time('Y/m/d');?>
					</div>
                </div>
                <div class="post-description"><?php the_content(); ?></div>                      
            </div><!-- end post-row-content -->
        </div><!-- end primary-row -->
        <?php endwhile; ?>    
        <div class="primary-row">
            <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
        </div>
    <?php endif; ?>
    <?php wp_reset_query(); ?>     
<?php get_template_part('footer'); ?>