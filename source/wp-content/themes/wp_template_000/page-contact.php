<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>
	<div class="primary-row clearfix">
		<h2 class="h2_title"><?php the_title(); ?></h2>
		<p class="pl20 pb20">
			料金やコースについてご不明な点がございましたら、こちらよりお問い合わせください。<br />
			当店スタッフより折り返しご連絡させていただきます。
		</p>
		
		<div class="contact-form">			
			<?php echo do_shortcode('[contact-form-7 id="248" title="無題"]') ?>
			<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
			<script type="text/javascript">
				$(document).ready(function(){
					$('#zip').change(function(){					
						//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
	    				AjaxZip3.zip2addr(this,'','pref','addr1','addr2');
	  				});
				});
			</script>			
		</div>
	</div>	
<?php get_footer(); ?>