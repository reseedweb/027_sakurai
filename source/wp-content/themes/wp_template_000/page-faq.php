<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">よくあるご質問</h2>
		<div class="box-content clearfix"><!-- begin box-content -->
			<p class="ln15em">
				Lei Lilium(レイリリウム)に寄せられるご質問の中で、特に多いものをご紹介いたします。
				はじめてエステをご利用になられる方はご参考にしてくださいませ。
			</p>
			<div class="faq-content-info"><!-- begin faq-content-info-->
				<p class="title">よくあるご質問一覧</p>
				<div class="text">
					<p><a href="#faq1" style="color:#333;">料金・ご予約について</a></p>
					<div class="text-under ln15em">
						<a href="#faq1_01">・入会金・年会費は必要ですか？</a><br />
						<a href="#faq1_02">・エステは料金が高そうで少し不安なのですが。</a><br />
						<a href="#faq1_03">・クレジットカードは使えますか？</a><br />
						<a href="#faq1_05">・予約の方法を教えてください。</a><br />
						<a href="#faq1_05">・予約をキャンセルしたいときはどうしたらいいですか？</a>
					</div>				
				</div><!-- end text -->
				
				<div class="text">
					<a href="#faq2" style="color:#333;">サービス内容について</a></p>
					<div class="text-under ln15em">						
						<a href="#faq2_01">・どんなコースがありますか？</a><br />
						<a href="#faq2_02">・どのコースが自分に合っているかわからないのですが、どうしたらいいですか？</a><br />
						<a href="#faq2_03">・オプションメニューだけ施術してもらうことはできますか？</a><br />
						<a href="#faq2_04">・お試しコースはありますか？</a><br />
						<a href="#faq2_05">・エステにかかる時間はどれくらいですか？</a><br />
						<a href="#faq2_06">・キープメンバーシステムとはどんなシステムですか？</a><br />
						<a href="#faq2_07">・購入した化粧品は自宅へ持ち帰ることはできますか？</a><br />
						<a href="#faq2_08">・化粧品は何カ月ぐらい使えますか？</a><br />
						<a href="#faq2_09">・キープ化粧品を途中で変更することはできますか？</a><br />
						<a href="#faq2_10">・男性もお店でエステが受けられますか？</a><br />
						<a href="#faq2_11">・エステ後に、万一お肌に異常を感じた場合、どうすればいいですか？</a><br />
						<a href="#faq2_12">・未成年でもエステを受けることはできますか？</a><br />
						<a href="#faq2_13">・スタンダードコースとオプションメニュー以外のサービスはありますか？</a>
					</div>				
				</div><!-- end text -->
				
				<div class="text">
					<p><a href="#faq3" style="color:#333;">美容相談</a></p>
					<div class="text-under ln15em">												
						<a href="#faq3_01">・冬になると肌がカサカサして困ってしまいます。</a><br />
						<a href="#faq3_02">・日やけによるシミやソバカスを防ぐにはどうしたらいいですか？</a><br />
						<a href="#faq3_03">・毛穴の黒ずみが気になるんですが・・・</a><br />
						<a href="#faq3_04">・大人になったのに、まだニキビができます。しかも治りにくいんです・・・</a><br />
						<a href="#faq3_05">・疲れてきたり、夕方になると顔がむくんでしまうんです。</a>
					</div>				
				</div><!-- end text -->
				
				<div class="text">
					<p><a href="#faq4" style="color:#333;">その他</a></p>
					<div class="text-under ln15em">																		
						<a href="#faq4_01">・サロンへ持参した方がいいものはありますか？</a><br />
						<a href="#faq4_02">・肌が敏感なのですが、大丈夫でしょうか？</a><br />
						<a href="#faq4_03">・エステセラピストに興味があります。資料請求はできますか？</a>
					</div>				
				</div><!-- end text -->
				
			</div><!-- end faq-content-info -->			
		</div><!-- end box-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title" id="faq1">料金・予約について</h2>
		<div class="box-content clearfix"><!-- begin box-content -->
			<div class="faq-content pt10" id="faq1_01"><!-- begin faq-content -->
				<h3 class="faq-title">入会金・年会費は必要ですか？</h3>
				<div class="faq-text ln15em">
					入会金・年会費など、当サロンでは施術料・商品代金以外のお金は一切いただいておりません。
				</div>
			</div><!-- end faq-content -->	
			
			<div class="faq-content" id="faq1_02"><!-- begin faq-content -->
				<h3 class="faq-title">エステは料金が高そうで少し不安なのですが。</h3>
				<div class="faq-text ln15em">
					当サロンでは、まずお試しコースを受けて効果を実感していただき、その後お肌とご予算に合わせたコースを決めていただけ
					ます。お得なキープメンバーシステムもございますので、ご来店の際にご紹介させていただいています。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq1_03"><!-- begin faq-content -->
				<h3 class="faq-title">クレジットカードは使えますか？</h3>
				<div class="faq-text ln15em">
					はい。JCB・VISA・アメリカン・エキスプレスをご使用いただけます。」→「申し訳ありませんが、現金のみのお支払いとなっております。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq1_04"><!-- begin faq-content -->
				<h3 class="faq-title">予約の方法を教えてください。</h3>
				<div class="faq-text ln15em">
					ご予約はお電話（090-8520-8603）もしくは、WEB予約で受け付けております。<br />
					営業時間９:００〜１９:００の間にお電話ください。WEB予約をご希望の方はコチラよりお問い合わせ下さい。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq1_05"><!-- begin faq-content -->
				<h3 class="faq-title">予約をキャンセルしたいときはどうしたらいいですか？</h3>
				<div class="faq-text ln15em">
					ご予約を頂いているお時間は、お客様のためにベッドを空けてお待ちしております。<br />
					万が一、お客様のご都合でご予約をキャンセルされる場合、必ず事前にお電話下さい。その際、別日程でご予約を承ります。
				</div>
			</div><!-- end faq-content -->
		</div><!-- end box-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title" id="faq2">サービス内容について</h2>
		<div class="box-content clearfix"><!-- begin box-content -->
			<div class="faq-content pt10" id="faq2_01"><!-- begin faq-content -->
				<h3 class="faq-title">どんなコースがありますか？</h3>
				<div class="faq-text ln15em">
					当サロンには、50分間のスタンダードコースとオプションメニューがございます。肌のお悩みに合わせてコースやメニューを
					組み合わせていただくこともできます。
				</div>
			</div><!-- end faq-content -->	
			
			<div class="faq-content" id="faq2_02"><!-- begin faq-content -->
				<h3 class="faq-title">どのコースが自分にあっているかわからないのですが、どうしたらいいですか？</h3>
				<div class="faq-text ln15em">
					サロンではエステセラピストが施術前のカウンセリングをさせていただきます。その上で、お客様お一人おひとりの肌の状態
					に合わせたお手入れを提案させていただきます。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq2_03"><!-- begin faq-content -->
				<h3 class="faq-title">オプションメニューだけ施術してもらうことはできますか？</h3>
				<div class="faq-text ln15em">
					オプションメニューだけの提供はしておりません。 スタンダードコースと合わせることでより効果的なお手入れとしてお勧め
					しております。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq2_04"><!-- begin faq-content -->
				<h3 class="faq-title">お試しコースはありますか？</h3>
				<div class="faq-text ln15em">
					当サロンでは、お試し体験が可能です。 コチラのページにトライアルコースについて記載しております。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq2_05"><!-- begin faq-content -->
				<h3 class="faq-title">エステにかかる時間はどれくらいですか？</h3>
				<div class="faq-text ln15em">
					スタンダードコースは50分間ですが、施術前後のカウンセリングのお時間も含めますと、約90分間となります。オプション
					メニューを追加される場合は、その時間がプラスされます。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq2_06"><!-- begin faq-content -->
				<h3 class="faq-title">キープメンバーシステムとはどんなシステムですか？</h3>
				<div class="faq-text ln15em">
					サロンで使用するお客様専用の化粧品をご購入いただきキープメンバーになると、2回目以降スタンダードコースをメンバー
					価格2,160円(税込)でご提供するシステムです。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq2_07"><!-- begin faq-content -->
				<h3 class="faq-title">購入した化粧品は自宅へ持ち帰ることはできますか？</h3>
				<div class="faq-text ln15em">
					サロンで使用するキープ化粧品はお客様の化粧品ですので、ご要望があれば、いつでもご自宅へお持ち帰りいただけます。<br />
					また、一旦持ち帰られた化粧品を、次回ご来店の際にお持ちになり、サロンでの施術時にご利用いただくこともできます。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq2_08"><!-- begin faq-content -->
				<h3 class="faq-title">化粧品は何ヶ月ぐらい使えますか？</h3>
				<div class="faq-text ln15em">
					キープ化粧品をサロンでの施術のみに使い、月2回ご来店いただいた場合、約5カ月分(約10回分)お使いいただけます。<br />
					ただし、化粧品のブランドや種類による内容量の違いや、お客様の使用量によっても使用期間が異なってきます。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq2_09"><!-- begin faq-content -->
				<h3 class="faq-title">キープ化粧品を途中で変更することはできますか？</h3>
				<div class="faq-text ln15em">
					キープ化粧品はお客様の化粧品です。途中で変更をご希望の場合は、再度、別の化粧品をご購入いただきます。それまでご使
					用されていたキープ化粧品は、持ち帰ってご自宅でお使いいただけます。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq2_11"><!-- begin faq-content -->
				<h3 class="faq-title">エステ後に、万が一お肌に異常を感じた場合、どうすればいいですか？</h3>
				<div class="faq-text ln15em">
					サービスにより体調を崩したり、施術部位に異常を生じた場合には、直ちに当サロンへ連絡してください。合わせて、できる
					だけ早く医師への受診をお勧めします。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq2_12"><!-- begin faq-content -->
				<h3 class="faq-title">未成年でもエステを受けることはできますか？</h3>
				<div class="faq-text ln15em">
					エステを受けていただくことができます。ただし、親権者の方の同意が必要です。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq2_13"><!-- begin faq-content -->
				<h3 class="faq-title">スタンダードコースとオプションメニュー以外のサービスはありますか？</h3>
				<div class="faq-text ln15em">
					その他のサービスは行っておりません。また、お客様のご要望でもまつ毛パーマ、まつ毛エクステンション、アートメイ ク、
					ケミカルピーリングなどのサービスはお断りさせていただいております。
				</div>
			</div><!-- end faq-content -->
			
		</div><!-- end box-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title" id="faq3">美容相談</h2>
		<div class="box-content clearfix"><!-- begin box-content -->
			<div class="faq-content pt10" id="faq3_01"><!-- begin faq-content -->
				<h3 class="faq-title">冬になると肌がカサカサして困ってしまいます。</h3>
				<div class="faq-text ln15em">
					冬は特に、皮脂の分泌も少なくなり、肌は非常に乾燥した状態になります。たっぷりと水分を補給し、美容マッサージなどを
					して肌の生まれ変わりをスムーズにしましょう。
				</div>
			</div><!-- end faq-content -->	
			
			<div class="faq-content" id="faq3_02"><!-- begin faq-content -->
				<h3 class="faq-title">日やけによるシミやソバカスを防ぐにはどうしたらいいですか？</h3>
				<div class="faq-text ln15em">
					日やけによるシミやソバカス対策としてビタミンCは欠かせません。 ビタミンCはメラニン色素の生成をおさえ、コラーゲン
					組織の生成を助ける働きがあるので、紫外線による肌の老化を防ぐ効果があると言われています。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq3_03"><!-- begin faq-content -->
				<h3 class="faq-title">毛穴の黒ずみが気になるんですが・・・</h3>
				<div class="faq-text ln15em">
					毛穴の黒ずみは、過剰な皮脂と角栓と呼ばれる毛穴につまった汚れが主な原因です。1日の終わりには、油性の汚れはクレン
					ジングで、古い角質などの汚れはウォッシング（洗顔料）で落としましょう。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq3_04"><!-- begin faq-content -->
				<h3 class="faq-title">大人になったのに、まだニキビができます。しかも治りにくいんです・・・</h3>
				<div class="faq-text ln15em">
					大人のニキビと呼ばれる吹き出物は、ストレス・睡眠不足・生活リズムの乱れ・生理前などが原因で、一時的に皮脂分泌が多
					くなることによって生じます。まずは、毛穴がつまらないように洗顔をていねいに行い、肌を清潔に保ちましょう。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq3_05"><!-- begin faq-content -->
				<h3 class="faq-title">疲れてきたり、夕方になると顔がむくんでしまうんです。</h3>
				<div class="faq-text ln15em">
					余分な老廃物がからだの中にたまると、むくみとなって現れます。 血行を良くしてリンパ液の流れをスムーズにし、しっかり
					と老廃物を流しましょう。
				</div>
			</div><!-- end faq-content -->		
			
		</div><!-- end box-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title" id="faq4">その他</h2>
		<div class="box-content clearfix"><!-- begin box-content -->
			<div class="faq-content pt10" id="faq4_01"><!-- begin faq-content -->
				<h3 class="faq-title">サロンへ持参した方がいいものはありますか？</h3>
				<div class="faq-text ln15em">
					特にございません。施術用のエステウェア、エステ後のメイクアップ品もご用意しております。お客様で準備していただくも
					のはございませんので、お気軽にお越しください。
				</div>
			</div><!-- end faq-content -->	
			
			<div class="faq-content" id="faq4_02"><!-- begin faq-content -->
				<h3 class="faq-title">肌が敏感なのですが、大丈夫でしょうか？</h3>
				<div class="faq-text">
					当サロンでは、実際にお客様の肌を確認してから、お一人おひとりの肌に合わせたエステのカルテを作成します。まずはお気
					軽にお近くのサロンへお問い合わせください。
				</div>
			</div><!-- end faq-content -->
			
			<div class="faq-content" id="faq4_03"><!-- begin faq-content -->
				<h3 class="faq-title">エステセラピストに興味があるのですが。</h3>
				<div class="faq-text ln15em">
					当サロンLei Lilium(レイリリウム)では、一緒にお仕事を頑張ってくださるエステセラピストさんを募集しております！ まず
					はお気軽にお問い合わせください。
				</div>
			</div><!-- end faq-content -->
			
		</div><!-- end box-content -->
	</div><!-- end primary-row -->
	
<?php get_footer(); ?>