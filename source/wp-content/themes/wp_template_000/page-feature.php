<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">メナードフェイシャルサロン Lei Liliumとは？</h2>
		<div class="feature-content"><!-- begin feature-content -->
			<div class="message-right message-250 clearfix"><!-- begin message-250 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_img.jpg" alt="feature" />
				</div><!-- end image -->
				<div class="text feature-text ln15em">
					<span class="feature-icon">◆</span>当サロンは完全予約制なので、待ち時間はいりません。また、
					ベッドは2台ご用意しておりますので、ご家族やお友達とも一緒
					にご来店いただけます。<br />
					<span class="feature-icon">◆</span>桜井駅から徒歩10分の駅近サロンなので、お仕事帰りの方で
					も気軽にお立ち寄りいただけます。<br />
					<span class="feature-icon">◆</span>閑静な住宅街にあるエステサロンなので、普段の生活から離れ
					た癒しの時間をお過ごしいただけます。
				</div><!-- end text -->
			</div><!-- end message-250 -->
		</div><!-- end feature-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">Lei Liliumでは6つの安心をお約束します。</h2>
		<div class="box-content clearfix"><!-- begin box-content -->
			Lei lilium(レイリリウム)では、ご来店されるお客様に少しでも安心してエステを受けていただけるよう、６つの
			安心をお約束いたします。		
			<div class="message-group message-classic mt20"><!-- begin message-group -->
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col message-col345 clearfix"><!-- begin message-col345 -->
						<div class="box-info"><!-- begin box-info -->
							<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_box1.jpg" alt="message col" />
							<h3 class="box-title ln15em">エステを受けるための入会金は発生しません。</h3>							
							<div class="box-text ln15em">
								入会金・年会費など、 レイリリウムでは施術料
								・商品代金以外のお金は一切いただいておりま
								せん。本当に美しくなっていただくためだけの
								料金を頂戴しております。 お気軽にエステをご
								利用ください。
							</div>
						</div><!-- end box-info -->					
					</div><!-- end message-col -->
					<div class="message-col message-col345 clearfix"><!-- begin message-col345 -->
						<div class="box-info"><!-- begin box-info -->
							<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_box2.jpg" alt="message col" />
							<h3 class="box-title ln15em">完全予約制で、待ち時間はかかりません。</h3>							
							<div class="box-text-e1 ln15em">
								完全予約制ですので、お客様のプライベートを
								守ります。あなただけの最適なカウンセリング
								とリラクゼーションをご提供致します。
							</div>
						</div><!-- end box-info -->					
					</div><!-- end message-col -->		
				</div><!-- end message-row -->
				
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col message-col345 clearfix"><!-- begin message-col345 -->
						<div class="box-info"><!-- begin box-info -->
							<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_box3.jpg" alt="message col" />
							<h3 class="box-title ln15em">非チケット制で、一人ひとりにカウンセリング</h3>							
							<div class="box-text-e2 ln15em">
								お客様のご要望に応じて、お選びいただける料
								金システムとなっております。お客様のご要望
								を詳しくカウンセリングし、一人ひとりに合っ
								た施術プランをご案内致します。
							</div>
						</div><!-- end box-info -->					
					</div><!-- end message-col -->
					<div class="message-col message-col345 clearfix"><!-- begin message-col345 -->
						<div class="box-info">
							<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_box4.jpg" alt="message col" />
							<h3 class="box-title ln15em">駐車場があるのでお車でもご来店いただけます！</h3>							
							<div class="box-text-e1 ln15em">
								サロンの左側パーキングの１番、５番がご利用
								できますので、お車でもお気軽にご来店いただ
								くことができます！
							</div>
						</div><!-- end box-info -->					
					</div><!-- end message-col -->	
				</div><!-- end message-row -->
				
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col message-col345 clearfix"><!-- begin message-col345 -->
						<div class="box-info"><!-- begin box-info -->
							<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_box5.jpg" alt="message col" />
							<h3 class="box-title ln15em">化粧品などの強引な販売はいたしません。</h3>							
							<div class="box-text-e2 ln15em">
								お客様に安心して、気軽にエステをご体験いた
								だけますよう、 無理な化粧品販売や勧誘などは
								一切行っておりません。安心してご来店くださ
								い。
							</div>
						</div><!-- end box-info -->					
					</div><!-- end message-col -->
					<div class="message-col message-col345 clearfix"><!-- begin message-col345 -->
						<div class="box-info">
							<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_box6.jpg" alt="message col" />
							<h3 class="box-title ln15em">お得なメンバー制をご用意しております！</h3>							
							<div class="box-text ln15em">
								お得なメンバーシステムをご用意しております。
								エステで使うお客様専用の化粧品をそろえてキ
								ープメンバーになりますと、各コースのエステ
								が2,160円(税込)で受けることができます。
							</div>
						</div><!-- end box-info -->					
					</div><!-- end message-col -->	
				</div><!-- end message-row -->
				
			</div><!-- end message-group -->
		</div><!-- end box-content -->
	</div><!-- end primary-row -->
<?php get_footer(); ?>