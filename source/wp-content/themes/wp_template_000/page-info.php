<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">メナードフェイシャルサロン Lei Liliumとは？</h2>
		<div class="box-content clearfix"><!-- begin box-content -->
			<div class="message-right message-230 clearfix"><!-- begin message-230 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/info_content_img1.jpg" alt="info" />
				</div><!-- end image -->
				<div class="text ln15em">
					はじめまして、メナードフェイシャルサロン「Lei Lilium」オーナーの春
					日 幸子です。 店舗名「Lei Lilium」は「レイリリウム」と読みます。名前
					の由来は、レイ（Lei）…「主にハワイで用いられる首飾り」と、リリウ
					ム（Lilium）…「植物のユリの学名に因んで名付けられた小惑星の名前」
					から取り、宇宙の小惑星をお客様と私達に例え、首飾りの輪のような関
					係として繋がっていけたらという意味を込め、この名前を付けました。<br />
					店内は木の温もりが感じられるアットホームな雰囲気ですので、エステが
					初めてという方でもリラックスして施術を受けていただけます。<br />
					お肌のメンテナンスだけでなく、日々の家事やお仕事などによるストレス
					をリフレッシュして頂けるよう、心を込めておもてなしさせていただきま
					す。
				</div><!-- end text -->
			</div><!-- end message-230 -->
		</div><!-- end box-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">サロン概要</h2>
		<div class="info-content clearfix"><!-- begin info-content -->
			<table class="info-table">
				<tr>
					<th>【 サロン名 】</th>
					<td>メナードフェイシャルサロン　Lei Lilium(レイリリウム )</td>
				</tr>
				<tr>
					<th>【 所在地 】</th>
					<td>
						〒633-0061<br />
						奈良県桜井市 上之庄832-1
					</td>
				</tr>
				<tr>
					<th>【 ＴＥＬ 】</th>
					<td>090-8520-8603(オーナー直通)</td>
				</tr>
				<tr>
					<th>【 営業時間 】</th>
					<td>
						9:00 ～ 19:00（完全予約制）<br />
						※時間外のご希望は応相談
					</td>
				</tr>
				<tr>
					<th>【 定休日 】</th>
					<td>不定休</td>
				</tr>
				<tr>
					<th>【 駐車場 】</th>
					<td>
						あり（2台）<br />
						お店左側パーキングの１番、５番がご利用できます。
					</td>
				</tr>				
				<tr>
					<th>【 代表者 】</th>
					<td>春日&nbsp;&nbsp;幸子</td>
				</tr>
				<tr>
					<th>【 カード支払い 】</th>
					<td>不可</td>
				</tr>
				<tr>
					<th>【 事業内容 】</th>
					<td>フェイシャルエステ</td>
				</tr>
			</table>
		</div><!-- end info-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">サロン概要</h2>
		<div class="info-content clearfix"><!-- begin info-content -->			
			<iframe width="711" height="336" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps?f=q&amp;source=s_q&amp;hl=ja&amp;geocode=&amp;q=%E5%A5%88%E8%89%AF%E7%9C%8C%E6%A1%9C%E4%BA%95%E5%B8%82+%E4%B8%8A%E4%B9%8B%E5%BA%84832-1&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=42.03917,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=%E6%97%A5%E6%9C%AC,+%E5%A5%88%E8%89%AF%E7%9C%8C%E6%A1%9C%E4%BA%95%E5%B8%82%E4%B8%8A%E4%B9%8B%E5%BA%84%EF%BC%98%EF%BC%93%EF%BC%92%E2%88%92%EF%BC%91&amp;t=m&amp;z=14&amp;ll=34.516956,135.837241&amp;output=embed"></iframe>
			<p class="pt20">
				【 電車でのご来店 】<br />
				・最寄り駅・・・・・JR「桜井(奈良県)駅」　出口より徒歩約10分。
			</p>
			<p class="pt20 pb20">
				【 車でのご来店 】<br />
				・ 駐車場あり。お店左側パーキングの1番、5番がご利用できます
			</p>
		</div><!-- end info-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">店舗風景</h2>
		<div class="info-content clearfix"><!-- begin info-content -->
			<p>
				レイリリウム の店舗風景です。<br />
				右の画像をクリックすると、大きな画像に切り替わります。
			</p>
			<div class="info-picture pb20 clearfix"><!-- begin info-picture -->				
				<div id="mainalbum" class="text">
					<img src="<?php bloginfo('template_url'); ?>/img/content/info_main_img1.jpg" alt="info" />					
				</div>
				<div id="subalbum" class="image">
					<ul class="clearfix">
						<li>
							<a href="<?php bloginfo('template_url'); ?>/img/content/info_main_img1.jpg"><img src="<?php bloginfo('template_url'); ?>/img/content/info_sub_img1.jpg" alt="message col" /></a>						
						</li>
						<li>
							<a href="<?php bloginfo('template_url'); ?>/img/content/info_main_img2.jpg"><img src="<?php bloginfo('template_url'); ?>/img/content/info_sub_img2.jpg" alt="message col" /></a>						
						</li>
						<li>
							<a href="<?php bloginfo('template_url'); ?>/img/content/info_main_img1.jpg"><img src="<?php bloginfo('template_url'); ?>/img/content/info_sub_img3.jpg" alt="message col" /></a>						
						</li>
						<li>
							<a href="<?php bloginfo('template_url'); ?>/img/content/info_main_img2.jpg"><img src="<?php bloginfo('template_url'); ?>/img/content/info_sub_img4.jpg" alt="message col" /></a>						
						</li>
						<li>
							<a href="<?php bloginfo('template_url'); ?>/img/content/info_main_img1.jpg"><img src="<?php bloginfo('template_url'); ?>/img/content/info_sub_img5.jpg" alt="message col" /></a>						
						</li>
						<li>
							<a href="<?php bloginfo('template_url'); ?>/img/content/info_main_img2.jpg"><img src="<?php bloginfo('template_url'); ?>/img/content/info_sub_img6.jpg" alt="message col" /></a>						
						</li>
					</ul>
				</div>
			</div><!-- end inofo-picture -->
		</div><!-- end info-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">サロン概要</h2>
		<div class="info-content clearfix"><!-- begin info-content -->
			<div class="message-right message-200 clearfix"><!-- begin message-200 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/info_content_img2.jpg" alt="info" />
					<p class="text-center">春日 幸子</p>
				</div><!-- end image -->
				<div class="text ln15em">
					<p>
						<span class="space">【 メナードorレイリリウム との出会い 】</span><br />
						友人のメナードサロンに２年間通って、オーナーから、お誘いを受けました。
					</p>

					<p class="pt20">
						<span class="space">【レイリリウム の魅力 】</span><br />
						敷居が低くて、スタッフがとても明るく、いつもお客様のお肌を美しく輝ける
						ように、また、リフレッシュして頂けるように心を込めたおもてなしをしているところです。
					</p>
					
					<p class="pt20">
						<span class="space">【 エステセラピストという仕事のやりがい 】</span><br />
						お客様が綺麗に目覚められる瞬間です。
					</p>
					
					<p class="pt20">
						<span class="space">【 今後の目標 】</span><br />
						どんなお客様にも対応できるスキルと、お客様から信頼されるセラピストになることです。
					</p>
						
					<p class="pt20">
						<span class="space">【 お客様へひとこと 】</span><br />
						エステと聞いて、若い方だけが受ける特別なものなんて思っていませんか？<br />
						当サロンでは、幅広い年齢の方がご来店され、低価格で本格的なメナードエス
						テを受けることができます。<br />
						その効果をどうぞ、お客様御自身で体験してみてください。きっと、御満足し
						て頂けると思います。
					</p>
				</div><!-- end text -->
			</div><!-- end message-200 -->
			
			<div class="message-right message-200 mt20 clearfix"><!-- begin message-200 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/info_content_img3.jpg" alt="info" />
					<p class="text-center">春日 幸子</p>
				</div><!-- end image -->
				<div class="text ln15em">
					<p>					
						<span class="space">【 メナードorレイリリウム との出会い 】</span><br />
						友人のメナードサロンに２年間通って、オーナーから、お誘いを受けました。
					</p>

					<p class="pt20">
						<span class="space">【レイリリウム の魅力 】</span><br />
						敷居が低くて、スタッフがとても明るく、いつもお客様のお肌を美しく輝ける
						ように、また、リフレッシュして頂けるように心を込めたおもてなしをしているところです。
					</p>
					
					<p class="pt20">		
						<span class="space">【 エステセラピストという仕事のやりがい 】</span><br />
						お客様が綺麗に目覚められる瞬間です。
					</p>
					
					<p class="pt20">
						<span class="space">【 今後の目標 】</span><br />
						どんなお客様にも対応できるスキルと、お客様から信頼されるセラピストになることです。
					</p>
					
					<p class="pt20">
						<span class="space">【 お客様へひとこと 】</span><br />
						エステと聞いて、若い方だけが受ける特別なものなんて思っていませんか？<br />
						当サロンでは、幅広い年齢の方がご来店され、低価格で本格的なメナードエス
						テを受けることができます。<br />
						その効果をどうぞ、お客様御自身で体験してみてください。きっと、御満足して頂けると思います。
					</p>
				</div><!-- end text -->
			</div><!-- end message-200 -->
		</div><!-- end info-content -->
	</div><!-- end primary-row -->
<?php get_footer(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#subalbum a').click(function(){
			$('#mainalbum img').hide();
			$('#mainalbum img').attr('src',$(this).attr('href'));
			$('#mainalbum img').fadeIn();
			return false;
		});
	});
</script>