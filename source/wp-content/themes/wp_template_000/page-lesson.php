<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">美肌レッスン・スキンチェック</h2>
		<div class="box-content clearfix"><!-- begin box-content -->
			<p class="ln15em">
				メナードフェイシャルサロン Lei Liliumでは、クレンジングからメイクまで、正しいお手入れ方法を学べるレッス
				ンを毎月行っております。 洗顔の泡立て方法からフルメイクまで、メナード化粧品を使用してご体験いただけます
				。 レッスンは少人数制ですので、お一人でもお友達とでも、お気軽にご参加くださいませ。
			</p>
		</div><!-- end box-content -->
		<p class="pt20">
			<img src="<?php bloginfo('template_url'); ?>/img/content/lesson_content_img.jpg" alt="lesson" />
		</p>		
	</div><!-- end primary-row -->
		
	<div class="mt20 clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">スキンチェック</h2>
		<div class="lesson-content clearfix"><!-- begin lesson-content -->
			<div class="message-left message-174 clearfix"><!-- begin message-174 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/lesson_content_img1.jpg" alt="lesson" />
				</div><!-- end image -->
				<div class="text ln15em">
					<h3 class="title">科学の力で本当の肌分析</h3>
					<p class="pt20 ln15em">
						キレイに見えても、実は肌の力が弱かったり、表に現れていない問題が潜んでいた
						り・・・。 メナードは、数百万を超える肌の解析データをもとに、精度の高い解析
						方法を確立。 あなたの肌の状態や肌力を科学的に分析し、最適なケア方法をアドバ
						イスいたします。
					</p>
				</div><!-- end text -->
			</div><!-- end message-174 -->
			<div class="clearfix">
				<div class="step-info clearfix"><!-- begin step-info -->
					<div class="step">STEP1　角質チェック</div>
					<p class="pt10">
						肌表面の角質細胞を採取。スキンチェックシートの質問におこたえいただきます。
					</p>
				</div><!-- end step-info -->
				
				<div class="step-info clearfix"><!-- begin step-info -->
					<div class="step">STEP2　科学的に分析</div>
					<p class="pt10">
						あなたの角質細胞を4つの視点から分析し、今の「美肌レベル」を判定します。
					</p>
					<div class="step-text mt10"><!-- begin step-text -->
						<div class="step-to-step">
							① うるおいを保つ力
							<p class="space">角質細胞のはがれ方を分析、肌のうるおいを保つ力がわかります。</p>
						</div>
						<div class="step-to-step">
							② 新陳代謝の状態
							<p class="space">角質細胞の大きさや形で、肌の新陳代謝の状態がわかります。</p>
						</div>
						<div class="step-to-step">
							③ シミ予備軍
							<p class="space ln15em">肌の中に隠れたシミ予備群からシミになる可能性や、メラニン量から紫外線ダメージの受けやすさがわかります。</p>
						</div>
						<div class="step-to-step">
							④ バリア機能
							<p class="space">バリア機能が弱いと、肌の調子が悪くなりやすいことがあります。</p>
						</div>
					</div><!-- end step-text -->
				</div><!-- end step-info -->
				
				<div class="step-info clearfix"><!-- begin step-info -->
					<div class="step">STEP3　最適なケアをアドバイス</div>
					<p class="pt10 ln15em">
						作成された、あなただけのアドバイスシートをもとに、スタッフが最適なお手入れ方法を アドバイスいたします。<br />
						4つの視点で肌を分析することで、あなたの肌が季節によってどう変化し、どんなケアをした ら良いかが、わかります。
					</p>
				</div><!-- end step-info -->			
			</div>				
		</div><!-- end lesson-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">カウンセリング</h2>
		<div class="lesson-content clearfix"><!-- begin box-content -->
			<div class="message-left message-174 clearfix"><!-- begin message-174 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/lesson_content_img2.jpg" alt="lesson" />
				</div><!-- end image -->
				<div class="text ln15em">
					<h3 class="title">もう迷わない。美肌へ一直線</h3>
					<p class="pt20 ln15em">
						科学的な分析データだけでは見えてこない問題も、お客様一人ひとりと向き合って
						お話しすることで見えてきます。スキンチェックで明らかになった美肌の育て方を
						、より確かなものにするためにも、メナードは、カウンセリングを大切にしています。
					</p>
				</div><!-- end text -->
			</div><!-- end message-174 -->
			<div class="clearfix">
				<div class="step-info clearfix"><!-- begin step-info -->
					<div class="step">的確なカウンセリングに自信</div>
					<p class="pt10 ln15em">
						豊富な美容知識を持ち、多くの女性たちの声に耳を傾けてきたメナードレディ（エステセラピスト）がおこなう
						カウンセリング。年齢もライフスタイルも異なる お客様と向き合い、信頼関係を築きながら、一緒に美肌を追求
						してきたメナードレディ（エステセラピスト）だから提供できるカウンセリングがあります。
					</p>
				</div><!-- end step-info -->
				
				<div class="step-info clearfix"><!-- begin step-info -->
					<div class="step">意外と多いお手入れへの誤解</div>
					<p class="pt10 ln15em">
						ちゃんとお手入れしているつもりでも、実は化粧品の力を活かしきれていないということがよくあります。洗顔
						やマッサージの方法、化粧水の使い方など、「こうすればもっと効果的」というポイントも、カウンセリングに
						よって初めてわかることなのです。
					</p>					
				</div><!-- end step-info -->
				
				<div class="step-info clearfix"><!-- begin step-info -->
					<div class="step">美肌への近道</div>
					<p class="pt10 ln15em">
						生活習慣のこと。食事のこと。人間関係のこと。一見お肌に関係ないようなことも、実は美肌に影響を与えてい
						ることがあります。メナードレディ（エステセラ ピスト）は、一緒にキレイを目指すパートナー。ちょっとした
						ことがきっかけで、美肌への近道が見つかることも。ぜひ、お気軽にご相談ください。
					</p>
				</div><!-- end step-info -->			
			</div>				
		</div><!-- end lesson-content -->
	</div><!-- end primary-row -->
	
	<div class="mt20 clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">美肌レッスン</h2>
		<div class="lesson-content clearfix"><!-- begin box-content -->
			<div class="message-left message-174 clearfix"><!-- begin message-174 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/lesson_content_img3.jpg" alt="lesson" />
				</div><!-- end image -->
				<div class="text ln15em">
					<h3 class="title">明日からお手入れが変わります</h3>
					<p class="pt20 ln15em">
						毎日のお手入れが、ちょっとやり方を変えるだけで、美肌へグンと近づくお手入れ
						になり、化粧品もその力を充分に発揮できるようになる。そんな正しいお手入れ方
						法を、楽しみながら覚えることができる。それがメナードの美肌レッスンです。
					</p>
				</div><!-- end text -->
			</div><!-- end message-174 -->
			<div class="clearfix">
				<div class="step-info clearfix"><!-- begin step-info -->
					<div class="step">いままでのお手入れとは全然違う？！正しいスキンケアレッスン</div>
					<p class="pt10 ln15em">
						美肌レッスンでは、メナードレディ（エステセラピスト）が、お手入れの正しい手順を丁寧にレッスン。お客様
						に普段のお手入れを実際にやっていただきながらポイントをお伝えするので、とてもわかりやすく、より良いお
						手入れ方法がしっかりと身につきます。きっと発見や驚きがいっぱいです。
					</p>
				</div><!-- end step-info -->
				<div class="step-info clearfix"><!-- begin step-info -->
					<div class="step">自分の肌で実感！レッスンの効果</div>
					<p class="pt10 ln15em">
						実際にレッスンを体験された方からは、「普段のお手入れの後と全然違う」「これなら美肌に近づけそう」といった
						、うれしい声をいただいています。また、毎日のお手入れの中で、ついうっかり忘れてしまうお手入れポイントを
						再確認できると、リピーターの方にも好評です。
					</p>
				</div><!-- end step-info -->
				
				<div class="step-info clearfix"><!-- begin step-info -->
					<div class="step">フェイシャルサロンだからこそできる！メナードの化粧品での美肌レッスン</div>
					<p class="pt10 ln15em">
						美肌レッスンは、メナードの化粧品を気軽にお試しいただく絶好の機会。ぜひレッスンを通して、その使い心地を
						、あなた自身の肌でお確かめください。
					</p>					
				</div><!-- end step-info -->						
			</div>				
		</div><!-- end lesson-content -->
	</div><!-- end primary-row -->
<?php get_footer(); ?>