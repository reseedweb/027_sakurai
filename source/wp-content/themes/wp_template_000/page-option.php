<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>
 
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">オプションメニューについて</h2>
		<div class="box-content clearfix">
			<div class="message-174 message-right clearfix"><!-- begin message-174 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img1.png" alt="option" />
				</div><!-- end image -->
				<div class="text ln15em">
					気になる部分は、スタンダードコースにオプションメニューをプラス。 お悩みに合
					わせたエッセンスを使った6色パックやデコルテまでケアするパックなど、全部で
					18種類をご用意。 お一人おひとりの肌のお悩みや状態に合わせて集中ケアを加える
					ことで、 さらに輝きに満ちた美しさへと導きます。
				</div><!-- end text -->
			</div><!-- end message-174 -->	
			<p>
				<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img2.png" alt="option" />
			</p>
		</div><!-- end box-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">エイジングケアオプションメニュー</h2>
		<div class="option-content clearfix"><!-- begin option-content -->
			<div class="option-row-info clearfix"><!-- begin option-row-info -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img3.png" alt="option" />
				</div><!-- end image -->
				<div class="text ln15em">
					<p>いつまでも美しく若々しい肌でいたいあなたに。大人気のオプションメニュー</p>
					<p class="space"><span style="color:#a52f1b;">エイジングケア</span>／1,080円（税込）</p>
					<p class="space">血液・リンパ・筋肉の3つにより効果的に働きかけ、女性の永遠のテーマ「いつまでも、美
					しく」にお応えする、自信のメニューです。</p>
				</div><!-- end text -->
			</div><!-- end option-row-info -->	
		</div><!-- end option-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">リラクゼーションオプションメニュー</h2>
		<div class="option-content clearfix"><!-- begin option-content -->
			<div class="option-row-info clearfix"><!-- begin option-row-info -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img4.png" alt="option" />
				</div><!-- end image -->
				<div class="text ln15em">
					<p>アロマの香りでリフレッシュ＆リラックスしたい方にオススメ</p>
					<p class="space"><span style="color:#a52f1b;">アロマリラクゼーション</span>／3,240円（税込）</p>
					<p class="space">アロママッサージオイルを使って背中・肩・首筋を丁寧に美容マッサージ。 深いリラクゼー
					ション効果で心も身体もリラックス。</p>
				</div><!-- end text -->
			</div><!-- end option-row-info -->	
		</div><!-- end option-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">オーセントマスクオプションメニュー</h2>
		<div class="option-content clearfix"><!-- begin option-content -->
			<div class="option-row-info clearfix"><!-- begin option-row-info -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img5.png" alt="option" />
				</div><!-- end image -->
				<div class="text ln15em">
					<p>ドラマチックな感動につつまれたい方にオススメ</p>
					<p class="space"><span style="color:#a52f1b;">オーセントマスク</span>／6,480円（税込）</p>
					<p class="space">顔と首・デコルテそれぞれの肌にうるおいとハリを与えるマスクシートタイプのパック。
					ラグジュアリーな満足感を。</p>
				</div><!-- end text -->
			</div><!-- end option-row-info -->	
		</div><!-- end option-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">パックオプションメニュー</h2>		
		<div class="option-content clearfix"><!-- begin option-content -->
			<p class="pb20">８種類のメニューの中から、あなたのお悩みに合ったものをセレクト</p>
			<p style="color:#a52f1b;">パックオプションメニュー</p>
			<p class="pt20">
				お悩みに合わせたエッセンスを肌になじませ、ずっしりとした重厚感が心地よいぷるぷるのアルゲタイプのパッ
				クをお顔にのせます。サロンならではの贅沢なスペシャルケアが、みずみずしく、うるおいのある肌へ導きます。
			</p>
			<div class="option-info clearfix"><!-- begin option-row-info -->
				<p class="text-center">
					それぞれの希望を叶えるオプションメニューをご用意
				</p>
				<ul class="clearfix">
					<li>
						<a href="#option01">肌のハリが気になる</a>
					</li>
					<li>
						<a href="#option03">肌の乾燥、カサつきが気になる</a>
					</li>
					<li>
						<a href="#option05">シミ、ソバカスが気になる</a>
					</li>
					<li>
						<a href="#option07">毛穴の汚れ、ベタつきが気になる</a>
					</li>
				</ul>
				
				<ul class="clearfix">
					<li>
						<a href="#option02">肌荒れ、ニキビが気になる</a>
					</li>
					<li>
						<a href="#option04">くすみが気になる</a>
					</li>
					<li>
						<a href="#option06">血行不良が気になる</a>
					</li>
					<li>
						<a href="#option08">デリケート肌に困っている</a>
					</li>
				</ul>
			</div><!-- end option-row-info -->

			<div id="option01" class="option-service clearfix"><!-- begin option-service -->
				<h4 class="option-title clearfix">
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon1.png" alt="option" />
					<span class="title">肌のハリが気になる方に</span>
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon2.png" alt="option" />					
				</h4>				
				<div class="message-group message-classic clearfix"><!-- begin message-group -->
					<div class="message-row clearfix"><!--message-row -->
						<div class="message-col message-col300">
							<h3 class="option-service-title">コラックスケアパック</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img6.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">ふっくらぷるぷる、ハリ弾む肌へ。</p>
									<p style="color:#ff0000;">2,700円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->	
						
						<div class="message-col message-col300">
							<h3 class="option-service-title">ネックケア(首もと)</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img7.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">年齢の出やすい首もとをシャープなネックラインに整えます。</p>
									<p style="color:#ff0000;">2,160円（税込））</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->
					</div><!-- end message-row -->
					
					<div class="message-row clearfix"><!--message-row -->
						<div class="message-col message-col300">
							<h3 class="option-service-title">アイトリートメント(目もと)</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img8.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">目もとにハリを持たせるふっくら集中ケア。</p>
									<p style="color:#ff0000;">2,160円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->	
						
					</div><!-- end message-row -->
				</div><!-- end message-group -->
			</div><!-- end option-service -->

			<div id="option02" class="option-service clearfix"><!-- begin option-service -->
				<h4 class="option-title clearfix">
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon1.png" alt="option" />
					<span class="title">肌荒れ・ニキビが気になる方に</span>
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon2.png" alt="option" />									
				</h4>							
				<div class="message-group message-classic clearfix"><!-- begin message-group -->
					<div class="message-row clearfix"><!--message-row -->
						<div class="message-col message-col300">
							<h3 class="option-service-title">ビューネケアパック</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img9.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">肌のコンディションを整えて、すこやかな肌へ。</p>
									<p style="color:#ff0000;">1,620円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->							
					</div><!-- end message-row -->
				</div><!-- end message-group -->
			</div><!-- end option-service -->
			
			<div id="option03" class="option-service clearfix"><!-- begin option-service -->
				<h4 class="option-title clearfix">
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon1.png" alt="option" />
					<span class="title">肌の乾燥・カサつきが気になる方に</span>
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon2.png" alt="option" />									
				</h4>						
				<div class="message-group message-classic clearfix"><!-- begin message-group -->
					<div class="message-row clearfix"><!--message-row -->
						<div class="message-col message-col300">
							<h3 class="option-service-title">モイストプールパック</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img10.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">肌をすこやかに整え、みずみずしくうるおった肌へ。</p>
									<p style="color:#ff0000;">2,160円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->							
					</div><!-- end message-row -->
				</div><!-- end message-group -->
			</div><!-- end option-service -->
			
			<div id="option04"class="option-service clearfix"><!-- begin option-service -->
				<h4 class="option-title clearfix">
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon1.png" alt="option" />
					<span class="title">くすみが気になる方に</span>
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon2.png" alt="option" />																		
				</h4>							
				<p class="pb10">※くすみとは古い角質により肌がくすんで見えることです。</p>
				<div class="message-group message-classic clearfix"><!-- begin message-group -->					
					<div class="message-row clearfix"><!--message-row -->						
						<div class="message-col message-col300">
							<h3 class="option-service-title">ダブルピタパック</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img11.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">くすみすっきり明るい肌へ。</p>
									<p style="color:#ff0000;">2,160円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->							
						<div class="message-col message-col300">
							<h3 class="option-service-title">ハーブピーリングパック</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img12.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">くすみ・汚れをハーブのつぶでピーリング。</p>
									<p style="color:#ff0000;">1,080円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->							
					</div><!-- end message-row -->
				</div><!-- end message-group -->
			</div><!-- end option-service -->
			
			<div id="option05" class="option-service clearfix"><!-- begin option-service -->
				<h4 class="option-title clearfix">
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon1.png" alt="option" />
					<span class="title">日焼けによるシミ、ソバカスが気になる方に</span>
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon2.png" alt="option" />					
				</h4>											
				<div class="message-group message-classic clearfix"><!-- begin message-group -->					
					<div class="message-row clearfix"><!--message-row -->						
						<div class="message-col message-col300">
							<h3 class="option-service-title">ホワイトニングパック</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img13.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">美白&保湿のW効果で肌本来の透明感へ。</p>
									<p style="color:#ff0000;">2,160円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->							
						<div class="message-col message-col300">
							<h3 class="option-service-title">パックホワイト</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img14.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">日やけによるシミ・ソバカスを防ぎます。</p>
									<p style="color:#ff0000;">1,080円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->							
					</div><!-- end message-row -->
				</div><!-- end message-group -->
			</div><!-- end option-service -->
			
			<div id="option06" class="option-service clearfix"><!-- begin option-service -->
				<h4 class="option-title clearfix">
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon1.png" alt="option" />
					<span class="title">血行不良が気になる方に</span>
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon2.png" alt="option" />										
				</h4>								
				<div class="message-group message-classic clearfix"><!-- begin message-group -->
					<div class="message-row clearfix"><!--message-row -->
						<div class="message-col message-col300">
							<h3 class="option-service-title">チョコフォームパック</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img15.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">チョコレートのようなふんわり泡で柔らかなもっちり肌に！</p>
									<p style="color:#ff0000;">2,160円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->							
					</div><!-- end message-row -->
				</div><!-- end message-group -->
			</div><!-- end option-service -->
			
			<div id="option07" class="option-service clearfix"><!-- begin option-service -->
				<h4 class="option-title clearfix">
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon1.png" alt="option" />
					<span class="title">毛穴の汚れ、ベタつきが気になる方に</span>
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon2.png" alt="option" />									
				</h4>				
				<div class="message-group message-classic clearfix"><!-- begin message-group -->					
					<div class="message-row clearfix"><!--message-row -->						
						<div class="message-col message-col300">
							<h3 class="option-service-title">クリーンパック</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img16.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">毛穴の汚れや余分な皮脂を吸着し、すべすべのなめらか肌へ。</p>
									<p style="color:#ff0000;">1,620円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->							
						<div class="message-col message-col300">
							<h3 class="option-service-title">ディープクレンジングパック</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img17.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">毛穴の汚れをクレイパックですっきり。</p>
									<p style="color:#ff0000;">1,080円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->							
					</div><!-- end message-row -->
					
					<div class="message-row clearfix"><!--message-row -->						
						<div class="message-col message-col300">
							<h3 class="option-service-title">毛穴ケア</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img18.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">毛穴の汚れをすっきり取り、毛穴をキュッと引き締めます。</p>
									<p style="color:#ff0000;">1,080円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->							
						<div class="message-col message-col300">
							<h3 class="option-service-title">スムースジェルパック</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img19.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">肌を引き締め、ハリとうるおいを与えて、なめらかな肌に。</p>
									<p style="color:#ff0000;">1,620円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->							
					</div><!-- end message-row -->
				</div><!-- end message-group -->
			</div><!-- end option-service -->
			
			<div id="option08" class="option-service mb20 clearfix"><!-- begin option-service -->
				<h4 class="option-title clearfix">
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon1.png" alt="option" />
					<span class="title">デリケート肌に困っている方に</span>
					<img src="<?php bloginfo('template_url'); ?>/img/content/option_icon2.png" alt="option" />																	
				</h4>								
				<div class="message-group message-classic clearfix"><!-- begin message-group -->
					<div class="message-row clearfix"><!--message-row -->
						<div class="message-col message-col300">
							<h3 class="option-service-title">マイルドスキンケアパック</h3>
							<div class="option-service-info">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/option_content_img20.png" alt="option" />
								</div><!-- end image -->
								<div class="text">
									<p class="pb10">デリケートな肌をやさしくいたわります。</p>
									<p style="color:#ff0000;">1,080円（税込）</p>
								</div><!-- end title -->
							</div>													
						</div><!-- end message-col -->							
					</div><!-- end message-row -->
				</div><!-- end message-group -->
			</div><!-- end option-service -->
			
		</div><!-- end option-content -->
	</div><!-- end primary-row -->	
<?php get_footer(); ?>