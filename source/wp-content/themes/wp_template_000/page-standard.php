<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>
<div class="primary clearfix">
</div>
	<div class="primary-row clearfix"><!--begin primary-row -->
		<h2 class="h2_title">スタンダードコースとは？</h2>
		<div class="message-left message-175 clearfix"><!-- begin message-175 -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_img1.jpg" alt="standard" />
			</div><!-- end image -->
			<div class="text ln15em">
				フェイシャルからデコルテまでの美容マッサージなど、リンパの流れやお肌の皮脂
				汚れなどをケアする11ステップのコースです。 お客様の肌に働きかけ、うるおい
				とやわらかさに満ちた本来の美しさを引き出します。 こころもからだも癒される、
				リラクゼーションのひとときをお楽しみください。
			</div><!-- end text -->
		</div><!-- end message-175 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">コース紹介</h2>
		<div class="box-content clearfix"><!--begin box-content -->
			<p>スタンダードコースはお肌の状態に合わせて3つのコースからお選びいただけます。
			詳しくは料金についてをご覧ください。</p>
			<ul class="standard-product1 clearfix"><!-- begin standard-product1 -->
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_product1_img1.jpg" alt="standard" />
					<span class="product1-title">イルネージュコース</span>
					<div class="product1-text">
						時の流れや紫外線によって失われていく肌のうるおいとハリを満たすコースです。
					</div>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_product1_img2.jpg" alt="standard" />
					<span class="product1-title">薬用リシアルコース</span>
					<div class="product1-text">
						うるおいに満ちた、みずみずしく透き通る柔肌をもたらすコースです。
					</div>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_product1_img3.jpg" alt="standard" />
					<span class="product1-title">つき華コース</span>
					<div class="product1-text">
						水密度を高めて、ぷるり弾む肌へ導くコースです。
					</div>
				</li>
			</ul><!-- end standard-product1 -->
		</div><!-- end box-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">スタンダードコースの流れ</h2>
		<div class="box-content clearfix"><!--begin box-content -->
			<p>スタンダードコースは、カウンセリングからローションまで、５０分間のコースとなっております。</p>
			<ul class="standard-step clearfix"><!-- begin standard-step -->
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_step01.jpg" alt="standard" />
					<span class="step-title">スキンチェック・カルテ作成</span>
					<div class="step-text">
						あなたの肌を分析すると共に素肌へのご要望をお伺いして、<br />
						一人ひとりに適切なエステをご提案いたします。
					</div>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_step02.jpg" alt="standard" />
					<span class="step-title">クレンジング</span>
					<div class="step-text">
						イオン化スチームをあて、毛穴の汚れを取ります。
					</div>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_step03.jpg" alt="standard" />
					<span class="step-title">ウォーミング</span>
					<div class="step-text">
						皮膚温を高めて、血液循環を活発にします。
					</div>
				</li>				
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_step04.jpg" alt="standard" />
					<span class="step-title">フェイシャル＆デコルテマッサージ</span>
					<div class="step-text">
						デコルテから首、顔全体のケアをし、フェイスラインをすっきり整えます。
					</div>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_step05.jpg" alt="standard" />
					<span class="step-title">バイブレーション</span>
					<div class="step-text">
						リンパの流れを促進し、むくみすっきり。肌をイキイキとさせます。
					</div>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_step06.jpg" alt="standard" />
					<span class="step-title">キッシング</span>
					<div class="step-text">
						毛穴につまった皮脂や汚れを吸引します。
					</div>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_step07.jpg" alt="standard" />
					<span class="step-title">ふきとり</span>
					<div class="step-text">
						スチームタオルでふきとります。
					</div>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_step08.jpg" alt="standard" />
					<span class="step-title">ローションパック</span>
					<div class="step-text">
						遠赤効果で皮膚温を上げながら、ローションパックで肌に水分を補給します。
					</div>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_step09.jpg" alt="standard" />
					<span class="step-title">スプレー</span>
					<div class="step-text">
						「薬用 ビューネ」をスプレーし、肌を引き締めます。
					</div>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_step10.jpg" alt="standard" />
					<span class="step-title">クーリング</span>
					<div class="step-text">
						肌を冷やし、ほてりをしずめ、キメを整えます。
					</div>
				</li>
				<li>
					<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_step11.jpg" alt="standard" />
					<span class="step-title">ローション・ミルクローション</span>
					<div class="step-text">
						水分・油分をしっかり補給します。
					</div>
				</li>				
			</ul><!-- end standard-step -->
		</div><!-- end box-content -->
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">料金について</h2>
		<div class="box-content clearfix"><!--begin box-content -->
			<p>コースには、１回ずつコース料金をお支払いいただく”ビジター”と、 メンパー価格でご利用が可能な”キープメ
			ンバー”の２つからお選びいただけます。</p>
			<p class="pt20"><img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_img2.png" alt="standard" /></p>
			<div class="price1">
				<div class="text ln2em">
					<p>イルネージュコースで比較すると、ビジターの場合、1回の施術料は6,480円(税込)です。 10回通
					ったとしたら、64,800円(税込)になります。 <br />
					キープメンバーの場合、施術で使用する化粧品のご購入が必要となりますので、 イルネージュコース
					の場合は初めに27,000円(税込)かかります。 しかし、1回の施術料が2,160円(税込)となるため、
					同じく10回通ったとしたら、 合計で48,600円(税込)になります。キープメンバーの化粧品は10回
					以上使用できるので、とってもお得な価格です。</p>

					<p class="pt20">（※お肌の状態や季節の変化により、化粧品の使用量には個人差があります。）
					（※一部のお店で施術料が異なる場合がございます。詳しくは各フェイシャルサロンにお問い合わせく
					ださい。）</p>
				</div>
			</div><!-- end price-text -->		
		</div><!-- end box-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">ビジターとキープメンバーの違い</h2>
		<div class="box-content clearfix"><!--begin box-content -->
			<p>イルネージュコースを月2回、5ヶ月通った場合</p>
			<p>【<span style="color: #a52f1b;">ビジター</span>の場合】</p>
			<div class="price2">
				<img src="<?php bloginfo('template_url'); ?>/img/content/standard_content_img3.png" alt="standard" />
				<div class="text">【<span style="color: #a52f1b;">キープメンバー</span>の場合】</div>
			</div>
		</div><!-- end box-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">お客様にご購入いただく商品</h2>
		<div class="box-content clearfix"><!--begin box-content -->
			<p>
				メナードフェイシャルサロンのキープ基礎化粧品のラインナップです。<br />
				キープメンバーになるには、こちらの基礎化粧品をセットでご購入いただく必要があります。
			</p>			
			<div class="standard-product2"><!-- begin standard-product2 -->
				<img src="<?php bloginfo('template_url'); ?>/img/content/standard_product2_img1.png" alt="standard" />
				<div class="product2-title">イルネージュ（全2品）／27,000円(税込)</div>
				<div class="product2-text">
					<p class="mb10">いつまでも鏡に映る自分の肌に自信が持てるのはとても素敵な
					こと。 イルネージュは、時の流れや紫外線によって失われてい
					く肌のうるおいとハリを満たすスキンケア。いつまでも自信が
					持てる、あこがれの肌運命をあなたへ。
					</p>
					<span class="set-product">セット内容(全2品)</span>
					<p class="pt10">
						<span class="icon">◆</span> ローション　12,960円（税込）<br />
						<span class="icon">◆</span> リフレッシュマッサージ　14,040円（税込)
					</p>
				</div>
			</div><!-- end standard-product2 -->
			
			<div class="standard-product2"><!-- begin standard-product2 -->
				<img src="<?php bloginfo('template_url'); ?>/img/content/standard_product2_img2.png" alt="standard" />
				<div class="product2-title">薬用リシアル（全3品）／18,900円(税込)</div>
				<div class="product2-text">
					<p class="mb10">
						水密度。それは美しい肌の源です。薬用リシアルは、肌の水密
						度を高めることで、ぷるり弾む肌へ導きます。 肌が満たされる
						と、ココロがしなやかに弾みだす。表面的ではない、湧き上が
						る美しさを目指すあなたへ。
					</p>
					<span class="set-product">セット内容(全3品)</span>
					<p class="pt10">						
						<span class="icon">◆</span>クレンジングクリーム　5,940円（税込）<br />
						<span class="icon">◆</span>マッサージクリーム　6,480円（税込）<br />
						<span class="icon">◆</span>ローション＜モイスト＞　6,480円（税込）<br />
						※全品＜医薬部外品＞
					</p>
				</div>
			</div><!-- end standard-product2 -->
			
			<div class="standard-product2"><!-- begin standard-product2 -->
				<img src="<?php bloginfo('template_url'); ?>/img/content/standard_product2_img3.png" alt="standard" />
				<div class="product2-title last-child">つき華（全3品）／9,180円(税込)</div>
				<div class="product2-text">
					<p class="mb10">
						華麗な大輪の花を短時間で咲かせるほどのエネルギーに満ちあ
						ふれた月下美人の花。 みずみずしくなめらかに肌を満たすスキ
						ンケアと、生き生きと輝く肌に仕立てるメイクアップのグルー
						プです。
					</p>
					<span class="set-product">セット内容(全2品)</span>
					<p class="pt10">						
						<span class="icon">◆</span>クレンジングクリーム　2,700円（税込）<br />
						<span class="icon">◆</span>マッサージクリーム　3,240円（税込）<br />
						<span class="icon">◆</span>ローション＜しっとり＞or＜さっぱり＞　3,240円（税込）
					</p>
				</div>
			</div><!-- end standard-product2 -->
		</div><!-- end box-content -->		
	</div><!-- end primary-row -->
	
<?php get_footer(); ?>