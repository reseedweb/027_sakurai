<?php get_header('header'); ?>	
<?php get_template_part('part','breadcrumb'); ?>
    <div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">お問い合わせありがとうございます</h2>
		<p class="pl20">
			お問い合わせいただきありがとうございます。<br />
			3営業日以内に、ご連絡させていただきます。		
		</p>
    </div><!-- end primary-row -->	
<?php get_footer('footer'); ?>