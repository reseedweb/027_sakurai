<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">エステセラピストに興味をお持ちの方</h2>
		<div class="box-content"><!-- begin box-content -->
			<p class="ln15em">
				Lei Liliumでは、お客様に美をお届けするエステセラピストを募集しております。 興味はあるけど未経験じゃ無理
				だし…という方でも安心！メナードフェイシャルサロンLei Liliumでは安心のサポート体制で、お子様のいるママ
				や、掛け持ちでお仕事をされている方でもエステセラピストになることができます。<br />
				一人前のエステセラピストを目指すためのレッスンなど、初心者からでもしっかりステップアップしていくこと
				ができ、エステセラピストになってからも自分の空いたお時間で無理のない働き方ができます。 <br />
				夢を実現したい、目標を持って働きたい、美に関わるお仕事をしたいという方は、ぜひ一度お問い合わせくださ
				いませ。
			</p>
		</div><!-- end box-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">エステセラピストとは？</h2>
		<div class="box-content"><!-- begin box-content -->
			<p class="ln15em">
				エステを通してお客様にキレイをご提案するのが、エステセラピストの仕事です。 メナードレディとして登録後、
				エステセラピストになるための本格的なエステの研修を受け、技術を身につけることができます。 試験に合格する
				と、エステセラピストとしてメナードのディプロマが授与されます。 ノルマはなく、フェイシャルサロンに出勤す
				る日時は自分のスケジュールに合わせることができますので、子育てや家事に忙しい方でも多くの方が、エステ
				セラピストとして活躍しています。
			</p>
		</div><!-- end box-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">ビューティアドバイザー（メナードレディ）とは？</h2>
		<div class="therapist-content clearfix"><!-- begin therapist-content -->
			<p class="pb20 ln15em">
				ビューティアドバイザー（メナードレディ）とは、主にお客様の健康状態や年齢などそれぞれのニーズに応じた
				スキンケアに関するアドバイスを行います。ノルマなどはなく出勤する必要もありませんので、子育てや家事に
				忙しい方・掛け持ちで仕事をされている方でも無理なく自分のペースで取り組むことができます。 さらに、スキ
				ンケアやフェイシャルエステ、メイクアップの技術を本格的に習得するための研修も用意されていますので、お
				客様へ美しさの提案ができるスキルを身につけることができます。
			</p>
			<div class="message-group message-classic"><!-- begin message-group -->
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col message-col345 therapist-list clearfix"><!-- begin message-col345 -->
						<div class="therapist-image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/therap_content_img1.jpg" alt="message col" />
							<h3 class="therapist-title">美肌レッスン</h3>
						</div>
						<div class="pt20 ln15em">
							お客様に、スキンケアの方法・化粧品の正しい使用量
							・使う順番・使い方などを楽しく体感いただき、スキ
							ンケアのポイントやホームケア方法をマスターしてい
							ただくレッスンです。 美容のプロとして、美肌作りの
							コツをお伝えします。
						</div>							
					</div><!-- end message-col -->	
					<div class="message-col message-col345 therapist-list clearfix"><!-- begin message-col345 -->
						<div class="therapist-image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/therap_content_img2.jpg" alt="message col" />
							<h3 class="therapist-title">スキンチェック</h3>
						</div>
						<div class="pt20 ln15em">
							スキンチェックキットを使ってお客様一人ひとりの肌
							を科学的にチェック。肌状態に合わせた最適なお手入
							れ方法をアドバイス。 スキンケア、ベースメイク、食
							生活提案といった外面、内面からトータルでのカウン
							セリングをおこないます。
						</div>							
					</div><!-- end message-col -->		
				</div><!-- end message-row -->	
				
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col message-col345 therapist-list clearfix"><!-- begin message-col345 -->
						<div class="therapist-image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/therap_content_img3.jpg" alt="message col" />
							<h3 class="therapist-title">ご自宅エステ</h3>
						</div>
						<div class="pt20 ln15em">
							肌と心に癒しをもたらすメナードのエステ。 アルフェ
							ストハンディライトという機器を使って、お客様のご
							自宅でフェイシャルエステサービスをおこないます。
						</div>							
					</div><!-- end message-col -->	
					<div class="message-col message-col345 therapist-list clearfix"><!-- begin message-col345 -->
						<div class="therapist-image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/therap_content_img4.jpg" alt="message col" />
							<h3 class="therapist-title">メイクアップ</h3>
						</div>
						<div class="pt20 ln15em">
							お客様お一人おひとりの好みや顔立ちに合わせて、お
							客様に合わせたメイクのアドバイス・テクニックをお
							伝えします。
						</div>							
					</div><!-- end message-col -->		
				</div><!-- end message-row -->		
				
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col message-col345 therapist-list clearfix"><!-- begin message-col345 -->
						<div class="therapist-image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/therap_content_img5.jpg" alt="message col" />
							<h3 class="therapist-title">商品お届け</h3>
						</div>
						<div class="pt20 ln15em">
							ふれあいを大切にし、お客様のご自宅、職場へ責任を
							もって商品をお届けします。<br />
							お届け先の範囲：桜井市近郊
						</div>							
					</div><!-- end message-col -->						
				</div><!-- end message-row -->		
				
			</div><!-- end message-group -->
		</div><!-- end therapist-content -->
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">エステセラピスト(メナードレディー)募集内容</h2>
		<div class="box-content clearfix"><!-- begin box-content -->
			<table class="therapist-table">
				<tr>
					<th><span class="space">給</span>与</th>
					<td>:</td>
					<td>完全歩合制（フルコミッション制）</td>
				</tr>
				<tr>
					<th><span class="space">対</span>象</th>
					<td>:</td>
					<td>メナードフェイシャルサロン Lei Lilium近隣の方。</td>
				</tr>
				<tr>
					<th><span class="space">時</span>間</th>
					<td>:</td>
					<td>自分の好きな時間に働けます</td>
				</tr>
				<tr>
					<th>仕事内容</th>
					<td>:</td>
					<td>エステ施術・化粧品販売</td>
				</tr>
			</table>
			<p>
				募集内容の詳細は、直接当店にお電話の上、お問い合わせください。 エステセラピスト、メナードレディにご興
				味をお持ちになられましたら、是非一度ご来店下さい。 疑問や不安を解消できますよう、詳細をご説明させて頂 きます。
			</p>
		</div><!-- end box-content -->
	</div><!-- end primary-row -->
<?php get_footer(); ?>