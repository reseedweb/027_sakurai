<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">初めての方限定！お得なトライアルチケット</h2>
		<div class="box-content clearfix"><!-- begin box-content -->
			<p class="ln15em">
				チケットご利用の際のご注意<br />
				１. チケットを利用する際の条件を必ずご確認ください。<br />
				２. チケットの内容は予告なく変更される場合があります。ご利用の際にコース等は、事前にご確認ください。
			</p>
		</div><!-- end box-content -->
		<p class="pt20">
			<img src="<?php bloginfo('template_url'); ?>/img/content/trial_content_img1.jpg" alt="trial" />
		</p>		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">スキンチェック</h2>
		<div class="lesson-content clearfix"><!-- begin lesson-content -->
			<div class="message-right message-174 clearfix"><!-- begin message-174 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/trial_content_img2.jpg" alt="trial" />
				</div><!-- end image -->
				<div class="text ln15em">
					<h3 class="title">お試し価格で気軽に、初めての美肌体験</h3>
					<p class="pt20 ln15em">
						メナードフェイシャルサロンLei Liliumでは、はじめてご来店されるお客さまに気
						軽にエステを楽しんでいただくため、 とってもお得なお試し価格でエステを試せる
						トライアルチケットをご用意しております。 こちらをご利用になりますと、メナー
						ド化粧品「イルネージュ」を使用したスタンダードコースを ご体験いただけます。<br />
						はじめての方はぜひ一度、こちらを利用して美肌を実感してみてくださいませ。
					</p>
				</div><!-- end text -->
			</div><!-- end message-174 -->			
		</div><!-- end lesson-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">カウンセリング</h2>
		<div class="lesson-content clearfix"><!-- begin box-content -->
			<div class="message-right message-154 clearfix"><!-- begin message-154 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/trial_content_img3.jpg" alt="trial" />
				</div><!-- end image -->
				<div class="text ln15em">
					<h3 class="title"><span class="pr20">STEP1</span>ご予約はお電話にて受け付けております。</h3>
					<p class="pt20 ln15em">
						メナードフェイシャルサロン ローズロックは完全予約制となっております。<br />
						必ず事前にお電話にてご予約をお願いします。<br />
						電話番号：090-8520-8603
					</p>
				</div><!-- end text -->
			</div><!-- end message-154 -->
			
			<div class="trial-arrow"><!-- begin trial-arrow -->
				<img src="<?php bloginfo('template_url'); ?>/img/content/trial_content_arrow.jpg" alt="trial" />
			</div><!-- end trial-arrow -->
			
			<div class="message-right message-154 clearfix"><!-- begin message-154 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/trial_content_img4.jpg" alt="trial" />
				</div><!-- end image -->
				<div class="text ln15em">
					<h3 class="title"><span class="pr20">STEP2</span>ご来店</h3>
					<p class="pt20 ln15em">
						施術用のエステウェア、エステ後のメイクアップ用品もご用意しております。<br />
						お客様に準備していただくものはございません。<br />
						普段の服装のままでお越しください。
					</p>
				</div><!-- end text -->
			</div><!-- end message-154 -->
			
			<div class="trial-arrow"><!-- begin trial-arrow -->
				<img src="<?php bloginfo('template_url'); ?>/img/content/trial_content_arrow.jpg" alt="trial" />
			</div><!-- end trial-arrow -->
			
			<div class="message-right message-154 clearfix"><!-- begin message-154 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/trial_content_img5.jpg" alt="trial" />
				</div><!-- end image -->
				<div class="text ln15em">
					<h3 class="title"><span class="pr20">STEP3</span>カウンセリング</h3>
					<p class="pt20 ln15em">
						エステ実施前にスキンチェック（肌分析）と、カウンセリングを行い、カルテを作成致します。<br />
						ご自身のお肌の調子や、フェイシャルコンディションの確認を致します。
					</p>
				</div><!-- end text -->
			</div><!-- end message-154 -->
			
			<div class="trial-arrow"><!-- begin trial-arrow -->
				<img src="<?php bloginfo('template_url'); ?>/img/content/trial_content_arrow.jpg" alt="trial" />
			</div><!-- end trial-arrow -->
			
			<div class="message-right message-154 clearfix"><!-- begin message-154 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/trial_content_img6.jpg" alt="trial" />
				</div><!-- end image -->
				<div class="text ln15em">
					<h3 class="title"><span class="pr20">STEP4</span>フェイシャルエステ施術</h3>
					<p class="pt20 ln15em">
						まずは、５０分間のスタンダードコースをご利用ください。<br />
						スタンダードコースのデコルテを含むフェイシャルエステは、オールハンドです。
					</p>
				</div><!-- end text -->
			</div><!-- end message-154 -->
			
			<div class="trial-arrow"><!-- begin trial-arrow -->
				<img src="<?php bloginfo('template_url'); ?>/img/content/trial_content_arrow.jpg" alt="trial" />
			</div><!-- end trial-arrow -->
			
			<div class="message-right message-154 mb20 clearfix"><!-- begin message-154 -->
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/trial_content_img7.jpg" alt="trial" />
				</div><!-- end image -->
				<div class="text ln15em">
					<h3 class="title"><span class="pr20">STEP5</span>アフターカウンセリング</h3>
					<p class="pt20 ln15em">
						お茶を飲みながら、ゆっくり時間をお過ごしください。<br />
						スキンケアや化粧品のご案内などをしながら、楽しくおしゃべりしましょう。
					</p>
				</div><!-- end text -->
			</div><!-- end message-154 -->						
		</div><!-- end lesson-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">Lei Liliumでは6つの安心をお約束します。</h2>
		<div class="box-content clearfix"><!-- begin box-content -->
			Lei lilium(レイリリウム)では、ご来店されるお客様に少しでも安心してエステを受けていただけるよう、６つの
			安心をお約束いたします。		
			<div class="message-group message-classic mt20"><!-- begin message-group -->
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col message-col345 clearfix"><!-- begin message-col345 -->
						<div class="box-info"><!-- begin box-info -->
							<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_box1.jpg" alt="message col" />
							<h3 class="box-title ln15em">エステを受けるための入会金は発生しません。</h3>							
							<div class="box-text ln15em">
								入会金・年会費など、 レイリリウムでは施術料
								・商品代金以外のお金は一切いただいておりま
								せん。本当に美しくなっていただくためだけの
								料金を頂戴しております。 お気軽にエステをご
								利用ください。
							</div>
						</div><!-- end box-info -->					
					</div><!-- end message-col -->
					<div class="message-col message-col345 clearfix"><!-- begin message-col345 -->
						<div class="box-info"><!-- begin box-info -->
							<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_box2.jpg" alt="message col" />
							<h3 class="box-title ln15em">完全予約制で、待ち時間はかかりません。</h3>							
							<div class="box-text-e1 ln15em">
								完全予約制ですので、お客様のプライベートを
								守ります。あなただけの最適なカウンセリング
								とリラクゼーションをご提供致します。
							</div>
						</div><!-- end box-info -->					
					</div><!-- end message-col -->		
				</div><!-- end message-row -->
				
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col message-col345 clearfix"><!-- begin message-col345 -->
						<div class="box-info"><!-- begin box-info -->
							<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_box3.jpg" alt="message col" />
							<h3 class="box-title ln15em">非チケット制で、一人ひとりにカウンセリング</h3>							
							<div class="box-text-e2 ln15em">
								お客様のご要望に応じて、お選びいただける料
								金システムとなっております。お客様のご要望
								を詳しくカウンセリングし、一人ひとりに合っ
								た施術プランをご案内致します。
							</div>
						</div><!-- end box-info -->					
					</div><!-- end message-col -->
					<div class="message-col message-col345 clearfix"><!-- begin message-col345 -->
						<div class="box-info">
							<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_box4.jpg" alt="message col" />
							<h3 class="box-title ln15em">駐車場があるのでお車でもご来店いただけます！</h3>							
							<div class="box-text-e1 ln15em">
								サロンの左側パーキングの１番、５番がご利用
								できますので、お車でもお気軽にご来店いただ
								くことができます！
							</div>
						</div><!-- end box-info -->					
					</div><!-- end message-col -->	
				</div><!-- end message-row -->
				
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col message-col345 clearfix"><!-- begin message-col345 -->
						<div class="box-info"><!-- begin box-info -->
							<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_box5.jpg" alt="message col" />
							<h3 class="box-title ln15em">化粧品などの強引な販売はいたしません。</h3>							
							<div class="box-text-e2 ln15em">
								お客様に安心して、気軽にエステをご体験いた
								だけますよう、 無理な化粧品販売や勧誘などは
								一切行っておりません。安心してご来店くださ
								い。
							</div>
						</div><!-- end box-info -->					
					</div><!-- end message-col -->
					<div class="message-col message-col345 clearfix"><!-- begin message-col345 -->
						<div class="box-info">
							<img src="<?php bloginfo('template_url'); ?>/img/content/feature_content_box6.jpg" alt="message col" />
							<h3 class="box-title ln15em">お得なメンバー制をご用意しております！</h3>							
							<div class="box-text ln15em">
								お得なメンバーシステムをご用意しております。
								エステで使うお客様専用の化粧品をそろえてキ
								ープメンバーになりますと、各コースのエステ
								が2,160円(税込)で受けることができます。
							</div>
						</div><!-- end box-info -->					
					</div><!-- end message-col -->	
				</div><!-- end message-row -->
				
			</div><!-- end message-group -->
		</div><!-- end box-content -->
	</div><!-- end primary-row -->
<?php get_footer(); ?>