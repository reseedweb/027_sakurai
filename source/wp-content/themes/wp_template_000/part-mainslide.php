<section id="mainslide">
    <div class="container"><!-- begin container -->
        <div class="row clearfix"><!-- begin row -->
            <div class="col-md-18"><!-- begin col -->
            	<div class="mainslide-content">
					<ul class="bxslider">						
						<li><img src="<?php bloginfo('template_url'); ?>/img/common/header_banner1.png" /></li>
						<li><img src="<?php bloginfo('template_url'); ?>/img/common/header_banner2.png" /></li>
						<li><img src="<?php bloginfo('template_url'); ?>/img/common/header_banner3.png" /></li>
					</ul>					
				</div>                
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section>
