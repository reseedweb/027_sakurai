<nav class="globalNavi-dynamic">
    <div class="container"><!-- begin container -->
        <div class="row clearfix"><!-- begin row -->
            <div class="col-md-18"><!-- begin col -->
            	<div class="globalNavi-content">					
            		<ul>
            			<li>
							<a href="<?php bloginfo('url'); ?>/">
								<img alt="top" src="<?php bloginfo('template_url');?>/img/common/navi_img1.jpg" /> 
							</a>
						</li>
            			<li>
							<a href="<?php bloginfo('url'); ?>/feature">
								<img alt="top" src="<?php bloginfo('template_url');?>/img/common/navi_img2.jpg" /> 
							</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/standard">
								<img alt="top" src="<?php bloginfo('template_url');?>/img/common/navi_img3.jpg" /> 
							</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/option">
								<img alt="top" src="<?php bloginfo('template_url');?>/img/common/navi_img4.jpg" /> 
							</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/info">
								<img alt="top" src="<?php bloginfo('template_url');?>/img/common/navi_img5.jpg" /> 
							</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/blog">
								<img alt="top" src="<?php bloginfo('template_url');?>/img/common/navi_img6.jpg" /> 
							</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/contact">
								<img alt="top" src="<?php bloginfo('template_url');?>/img/common/navi_img7.jpg" /> 
							</a>
						</li>						
            		</ul>						
				</div>                
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->	
</nav>