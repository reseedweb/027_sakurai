<?php get_template_part('header'); ?>
<div class="primary-row clearfix">
	<?php if(have_posts()): while(have_posts()) : the_post(); ?>
	<div class="primary-row clearfix">		
		<h2 class="h2_title"><?php the_title(); ?></h2>
		<div class="message-work clearfix">
			<div class="image">
				<?php the_post_thumbnail('large'); ?>
			</div>
			<div class="text">
				<?php echo get_field('customer_title', get_the_id()); ?>				
				<div class="voice-content">
					<?php echo get_field('customer_text', get_the_id()); ?>				
				</div>				
			</div>
		</div>
		<div class="mt10">
			<?php the_content(); ?>
		</div>
	</div><!-- end primary-row -->    
	<div class="primary-row clearfix">
	    <!-- post navigation -->
		<div class="navigation cf float">
		<?php if( get_previous_post() ): ?>
		<div style="float:left;"><?php previous_post_link('%link', '« %title'); ?></div>
		<?php endif;
		if( get_next_post() ): ?>
		<div style="float:right;"><?php next_post_link('%link', '%title »'); ?></div>
		<?php endif; ?>
		</div>
		<!-- /post navigation -->	
	</div><!-- end primary-row -->
	<?php endwhile;endif; ?>
</div><!-- end primary-row -->
<?php get_template_part('footer'); ?>