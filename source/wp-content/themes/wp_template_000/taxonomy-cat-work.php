<?php get_header(); ?>
<div class="primary-row clearfix">
	<h1 class="page-title">製作事例</h1>
	<div class="work-group-messages message-group clearfix">		
	<?php	
    $queried_object = get_queried_object();
    $term_id = $queried_object->term_id;	
	$work_posts = get_posts( 	array(
	    'post_type'=> 'work',
        'posts_per_page' => get_query_var('posts_per_page'),
        'paged' => get_query_var('paged'),		        
        
        'tax_query' => array(
            array(
            'taxonomy' => 'cat-work', 
            'field' => 'term_id', 
            'terms' => $term_id))
	));
	$i = 0;
	foreach($work_posts as $work_post):
	?>	
    <?php $i++; ?>
    <?php if($i%2 == 1) : ?>
	<div class="message-row clearfix">		
	<?php endif; ?>		
        <div class="message-col message-col375">        
			<h2><?php echo $work_post->post_title; ?></h2>
			<div class="image">
				<?php echo get_the_post_thumbnail($work_post->ID,'work'); ?>
			</div>
			<div class="work-ctf-table">
				<table>		
					<tr>
						<th>箱形状</th>
						<td><?php echo get_field('ctf_01'); ?></td>
					</tr>
					<tr>
						<th>厚み</th>
						<td><?php echo get_field('ctf_02'); ?></td>
					</tr>
					<tr>
						<th>表面色</th>
						<td><?php echo get_field('ctf_03'); ?></td>
					</tr>
					<tr>
						<th>印刷面</th>
						<td><?php echo get_field('ctf_04'); ?></td>
					</tr>
					<tr>
						<th>色数	</th>
						<td><?php echo get_field('ctf_05'); ?></td>
					</tr>																
				</table>
			</div>
			<div class="text">
				<?php echo get_field('description'); ?>
			</div>
		</div><!-- end col -->
    <?php if($i%2 == 0 || $i == count($posts) ) : ?>
    </div>
    <?php endif; ?>
	<?php endforeach; ?>
	</div>		
    <div class="primary-row text-center">
        <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
    </div>
    <?php wp_reset_query(); ?>	
</div>
<?php get_footer(); ?>